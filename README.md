# Описание

Webpack шаблон для создания одностраничного сайта (лэндинга) с 404 страницей, имеющий Service Worker для работы в оффлайн режиме.

Сжатие и оптимизация всего на свете присутствует.

# Установка

Поочередно выполняем команды:

- git clone https://msidorchyk@bitbucket.org/it_promedia/webpack-landing.git
- cd webpack-landing
- npm install


# Материалы

- Презентация - https://docs.google.com/presentation/d/1PDDvMEmnUadRXNc6iriggLlDBMlFsporUEBsfQlHvF0/edit?usp=sharing
- NodeJS - https://nodejs.org/en/
- NPM - https://www.npmjs.com/
- Sass - https://sass-lang.com/
- Webpack - https://webpack.js.org
- Bootstrap - https://getbootstrap.com/
